﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace JTTT
{
    public sealed class Logger
    {
        private const string LogFileName = "jttt.log";
        public static readonly Logger instance = new Logger();
        private StreamWriter writer;
        private Logger()
        {
            writer = new StreamWriter(LogFileName);
        }

        public static void addLogEntry(string entry)
        {
            instance.writer.WriteLine(entry);
            instance.writer.Flush();
        }
    }
}
