﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace JTTT
{
    [Serializable()]
    [XmlInclude(typeof(WeatherTask))]
    [XmlInclude(typeof(SiteTask))]
    public abstract class BaseTask
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string mail { get; set; }
        public virtual void execute() { }
        public override string ToString()
        {
            return name;
        }
    }
    
    public class WeatherTask : BaseTask
    {
        public WeatherTask() {}
        public string city { get; set; }
        public int temp { get; set; }
        public WeatherTask(string name, string mail, string city, int temp)
        {
            this.name = name;
            this.city = city;
            this.mail = mail;
            this.temp = temp;
        }

        public override void execute()
        {
            const string URL = "http://openweathermap.org/data/2.5/weather?q={0}";
            var weatherDownloader = new WeatherDownloader();
            weatherDownloader.download(String.Format(URL, city));
            var weather = weatherDownloader.parse();
            var tempStr = weather.main.temp-273.15 > temp ? "cieplo" : "zimno";
            Controller.instance.showWeatherWindow(weather, tempStr);
        }
    }
    public class SiteTask : BaseTask
    {
        SiteTask() {}
        public string URL { get; set; }
        public string text { get; set; }
        private const string defaultPassword = "supertajnehaslo";
        private const string defaultUserName = "testert955";

        public SiteTask(string name, string URL, string mail, string text)
        {
            this.name = name;
            this.URL = URL;
            this.mail = mail;
            this.text = text;
        }

        public override void execute()
        {
            var downloader = new HtmlDownloader(URL, text);
            Logger.addLogEntry(String.Format("Fetching image urls and alts from provided URL= {0}", URL));
            var matchingImgNodes = downloader.getPageImgNodes();
            Logger.addLogEntry("Download succesful.");

            var mailSender = new MailSender(mail, defaultUserName, defaultPassword);
            Logger.addLogEntry(String.Format("Sending fetched results to email = {0}", mail));
            mailSender.send(matchingImgNodes);
            Logger.addLogEntry(String.Format("Sending completed successfuly.", mail));
        }

    }
}
