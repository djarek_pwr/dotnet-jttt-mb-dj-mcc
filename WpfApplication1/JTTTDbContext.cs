﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace JTTT
{
    class JTTTDBInit : System.Data.Entity.DropCreateDatabaseIfModelChanges<JTTTDbContext>
    {
        protected override void Seed(JTTTDbContext context)
        {
            context.tasks.Add(new SiteTask("SiteTask", "http://demotywatory.pl", "testert955@gmail.com", "a"));
            context.tasks.Add(new WeatherTask("WeatherTask", "testert955@gmail.com", "Honolulu", 0));
            base.Seed(context);
        }
    }
    class JTTTDbContext : DbContext
    {
        public JTTTDbContext()
            : base("JTTT")
        {
            Database.SetInitializer<JTTTDbContext>(new JTTTDBInit());
        }
        public DbSet<BaseTask> tasks { get; set; }
    }
}