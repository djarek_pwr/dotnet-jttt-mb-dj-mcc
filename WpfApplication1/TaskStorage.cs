﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace JTTT
{
    public sealed class TaskStorage
    {
        private String fileName;
        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<BaseTask>));
        public TaskStorage(String fileName)
        {
            this.fileName = fileName;
        }

        public List<BaseTask> loadTaskList()
        {
            
            using (var reader = new System.IO.StreamReader(fileName))
            {
                Logger.addLogEntry("Serializing task list.");
                return serializer.Deserialize(reader) as List<BaseTask>;
            }
        }

        public void storeTaskList(List<BaseTask> taskList)
        {
            using (var writer = new System.IO.StreamWriter(fileName))
            {
                Logger.addLogEntry("Deserializing task list.");
                serializer.Serialize(writer, taskList);
            }
        }

        public List<BaseTask> loadTaskListFromDB()
        {
            var taskList = new List<BaseTask>();
            using (var ctx = new JTTTDbContext())
            {
                foreach (var task in ctx.tasks)
                {
                    taskList.Add(task);
                }
            }
            return taskList;
        }

        public void storeTaskListInDB(List<BaseTask> taskList)
        {
            
            using (var ctx = new JTTTDbContext())
            {
                ctx.Database.Connection.Open();
                ctx.tasks.RemoveRange(ctx.tasks);
                var allTasks = from t in ctx.tasks
                               select t;

                ctx.tasks.AddRange(taskList);
                ctx.SaveChanges();
            }
        }
    }
}
