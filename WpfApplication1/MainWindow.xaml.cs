﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;
using System.Net.Mail;


namespace JTTT
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool siteTaskTabActive = false;
        public MainWindow()
        {
            InitializeComponent();
            listBox.ItemsSource = Controller.instance.getTaskList();
        }

        private void ExecuteButtonClick(object sender, RoutedEventArgs e)
        {
            var task = listBox.SelectedItem as Task;
            Controller.instance.executeAll();
        }

        private void AddButtonClick(object sender, RoutedEventArgs e)
        {
            if (siteTaskTabActive)
            {
                if (String.IsNullOrEmpty(MailTextBox.Text) || String.IsNullOrEmpty(URLTextBox.Text) || String.IsNullOrEmpty(SearchedTextBox.Text)
                    || String.IsNullOrEmpty(TaskNameTextBox.Text))
                {
                    MessageBox.Show("Wypełnij wszystkie pola");
                    return;
                }
                Controller.instance.addTask(new SiteTask(TaskNameTextBox.Text, URLTextBox.Text, MailTextBox.Text, SearchedTextBox.Text));
            }
            else
            {
                Controller.instance.addTask(new WeatherTask(TaskNameTextBox.Text, MailTextBox.Text, CityTextEdit.Text, int.Parse(TemperatureTextEdit.Text)));   
            }
        }

        private void ClearButtonClick(object sender, RoutedEventArgs e)
        {
            var task = listBox.SelectedItem as SiteTask;

            Controller.instance.removeTask(task);
        }

        private void DeserializeButtonClick(object sender, RoutedEventArgs e)
        {
            Controller.instance.loadTaskList();
        }

        private void SerializeButtonClick(object sender, RoutedEventArgs e)
        {
            Controller.instance.saveTaskList();
        }

        private void OnWindowClose(object sender, EventArgs e)
        {
            Controller.instance.saveTasksInDb();
        }

        private void CheckPageTabFocused(object sender, RoutedEventArgs e)
        {
            var tab = sender as TabItem;
            if (tab == CheckPageTab)
            {
                siteTaskTabActive = true;
            }
            else
            {
                siteTaskTabActive = false;
            }
        }

    }
}
