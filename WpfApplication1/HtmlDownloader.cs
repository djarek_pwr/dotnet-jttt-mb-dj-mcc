﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
namespace JTTT
{

    public struct ImgNode
    {
        public ImgNode(string alt, string src)
        {
            this.alt = alt;
            this.src = src;
        }
        public string alt, src;
    }

    public class HtmlDownloader
    {
        private string url;
        private string text;
        public HtmlDownloader(string url, string text)
        {
            this.url = url;
            this.text = text;
        }

        /// <summary>
        /// Prosta metoda, która zwraca zawartość HTML podanej strony www
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetPageHtml()
        {
            using (WebClient wc = new WebClient())
            {
                // Pobieramy zawartość strony spod adresu url jako ciąg bajtów
                byte[] data = wc.DownloadData(url);
                // Przekształcamy ciąg bajtów na string przy użyciu kodowania UTF-8. To oczywiście powinno zależeć od właściwego kodowania
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));

                // Wersja uproszczona bez uwzględniania kodowania:
                // string html = wc.DownloadString(url);

                return html;
            }
        }

        public List<ImgNode> getPageImgNodes()
        {
            HtmlDocument doc = new HtmlDocument();

            // Używamy naszej metody do pobrania zawartości strony
            string pageHtml = GetPageHtml();

            // Ładujemy zawartość strony html do struktury documentu (obiektu klasy HtmlDocument)
            doc.LoadHtml(pageHtml);

            // Metoda Descendants pozwala wybrać zestaw node'ów o określonej nazwie
            var nodes = doc.DocumentNode.Descendants("img");
            var ret = new List<ImgNode>();
            // Iterujemy po wszystkich znalezionych node'ach
            foreach (var node in nodes)
            {
                var alt = node.GetAttributeValue("alt", "");
                var src = node.GetAttributeValue("src", "");
                if (alt.Contains(text))
                {
                    ret.Add(new ImgNode(alt, src));
                }
            }
            return ret;
        }
    }
}
