﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace JTTT
{
    public sealed class Controller
    {
        private BindingList<BaseTask> tasks= new BindingList<BaseTask>();
        private TaskStorage taskStorage = new TaskStorage("tasks.xml");
        private Controller() 
        {
            tasks.AllowNew = true;
            tasks.AllowRemove = true;
            tasks.AllowEdit = true;
            tasks.RaiseListChangedEvents = true;
        }

        public static readonly Controller instance = new Controller();

        public void addTask(BaseTask task)
        {
            tasks.Add(task);
        }

        public void executeTask(BaseTask task)
        {
            task.execute();
            tasks.Remove(task);
        }

        public void removeTask(BaseTask task)
        {
            tasks.Remove(task);
        }

        public void executeAll()
        {
            foreach (var task in tasks)
            {
                task.execute();
            }
            tasks.Clear();
        }

        public BindingList<BaseTask> getTaskList()
        {
            var tasksList = taskStorage.loadTaskListFromDB();
            foreach (var task in tasksList)
            {
                tasks.Add(task);
            }
            return tasks;
        }

        public void loadTaskList()
        {
            var list = taskStorage.loadTaskList();
            tasks.Clear();
            foreach (var task in list) {
                tasks.Add(task);
            }
        }

        public void saveTaskList()
        {
            taskStorage.storeTaskList(tasks.ToList());
        }

        public void saveTasksInDb()
        {
            taskStorage.storeTaskListInDB(tasks.ToList());
        }

        public void showWeatherWindow(WeatherObject weather, String tempStr)
        {
            const String description = "Pogoda w miescie {0}:\nJest {1}, {2} stopni C";
            var weatherWindow = new WeatherWindow();
            var formattedString = String.Format(description, weather.name, tempStr, weather.main.temp-273.15);
            weatherWindow.setWeatherDescription(formattedString);
            weatherWindow.Show();
        }
    }
}
