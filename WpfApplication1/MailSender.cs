﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
namespace JTTT
{
    class MailSender
    {
        private string address;
        private string userName;
        private string password;
        public MailSender(string address, string userName, string password)
        {
            this.address = address;
            this.userName = userName;
            this.password = password;
        }

        public void send(List<ImgNode> matchingNodes)
        {
            const string template = "Znaleziono obrazek:\nAdres= {0}\nOpis= {1}\n---------------------------\n";
            const string title = "JTTT v1.0 obrazki.";
            string body = "JTTT Lista znalezionych obrazkow:\n-------------------------------\n";
            foreach (var node in matchingNodes)
            {
                body += String.Format(template, node.src, node.alt);
            }
            var mail = new MailMessage(address, address, title, body);
            mail.BodyEncoding = UTF8Encoding.UTF8;
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            var client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Send(mail);
        }
    }
}
